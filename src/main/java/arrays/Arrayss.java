package arrays;

class Arrayss {

     int [] ar1;
     int [] ar2;

    public int [] getArray1 () {
        return ar1;
    }

    public int [] getArray2 () {
        return ar2;
    }

    public void setArray1 (int [] ar1) {
        this.ar1 = ar1;
    }

    public void setArray12 (int [] ar2) {
        this.ar2 = ar2;
    }

    public static int [] findTheSame(int [] arr1, int [] arr2) {
        int l1 = arr1.length;
        int l2 = arr2.length;
        int l3 = (l1 >= l2)? l1: l2;
        int [] ar3=new int[l3];
        int k = 0;
        for (int i = 0; i < l1; i++) {
            for (int j = 0; j < l2; j++) {
                if (arr1[i] == arr2[j]) {
                    ar3[k] = arr1[i]; k++; break;
                }
            }
        }

        for (int i = 0; i < l3; i++) {
            for (int j = i+1; j < l3; j++) { if(ar3[j] == ar3[i]) ar3[j] = 0;}
        }
        int [] ar4 = new int[l3];

        int less = 0;
        for (int i = 0; i < l3; i++) { if(ar3[i]!=0) ar4[i-less] = ar3[i];
        else if(ar3[i] == 0) less++;
        }
        return ar4;
    }

    public static int [] findInOne(int [] arr1, int [] arr2) {
        int l1 = arr1.length;
        int l2 = arr2.length;
        int [] ar3 = new int[l1+l2];
        int k = 0;
        boolean present = false;
        for (int i = 0; i < l1; i++) { //check numbers from first array in second one
            present=false;
            for (int j = 0; j < l2; j++) {
                if (arr1[i] == arr2[j]) present = true;
            }
            if (present == false) {ar3[k] = arr1[i]; k++;}
        }

        for (int i = 0; i<l2; i++) { //check numbers from second array in first one
            present = false;
            for (int j = 0; j < l1; j++) {
                if (arr2[i] == arr1[j]) present = true;
            }
            if (present == false) {ar3[k] = arr2[i]; k++;}
        }

        return ar3;
    }

    public static int [] meetMoreThenTwice(int [] arr) {
        int k0 = 0; //index of arr1 <array> - numbers found before
        int k = 0;  //index of arr2 <array> - numbers that found at least 3 times
        int zero=0; //frequency of 0 number in array
        boolean present = false;
        int [] arr1 = new int[arr.length];
        int [] arr2 = new int[arr.length];
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == 0) {zero++; continue; }
            for (int l = 0; l < arr1.length; l++) {if (arr1[l] == arr[i]) {present = true;}} //check if we found this number before in initial array
            if (present == false)  {arr1[k0] = arr[i];  //if we doesnot found the number - add it to second array (arr1) with unique numbers of initial array
                int frequency = 1;  //we found arr[i] once
                for (int j = i+1; j < arr.length; j++) {
                    if (arr[j] == arr1[k0]) frequency++;  //we found arr[i] (arr1[0]) again
                }
                if (frequency > 2) {arr2[k] = arr[i]; k++;}  //we add number that meets more then 2 times to third array <arr2>
                k0++; }
            present = false;
        }
        return arr2;
    }

    public static int [] eliminateSequence (int [] arr) {

        boolean zero = false;
        int [] arr1 = new int[arr.length];
        arr1[0] = arr[0];
        int k = 1;
        //int length=0;
        //int current=arr[0];
        for (int i = 1; i < arr.length; i++) {
            if (arr[i] != arr[i-1]) {
                arr1[k] = arr[i]; k++; }
        }
        return arr1;
    }

    public static void main (String args[]) {
        int [] ara = {9,1,2,5,3,2,4};
        int [] arb = {2,3,7,1,2,8,1,3};
        int [] arc = findTheSame(ara, arb);
        System.out.println(arc[0]+" "+arc[1]+" "+ arc[2]+" "+ arc[3]+" "+ arc[4]+" "+ arc[5]);
        int [] arc1 = findInOne(ara, arb);
        System.out.println(arc1[0]+" "+arc1[1]+" "+ arc1[2]+" "+ arc1[3]+" "+ arc1[4]+" "+ arc1[5]);
        int [] ard = {1,0,0,4,4,4,1,3,3,3,0,0,2,2,4,3,3,6,7};
        int [] arc2 = meetMoreThenTwice(ard);
        System.out.println(arc2[0]+" "+arc2[1]+" "+ arc2[2]+" "+ arc2[3]+" "+ arc2[4]+" "+ arc2[5]);
        int [] arc3 = eliminateSequence(ard);
        System.out.println(arc3[0]+" "+arc3[1]+" "+ arc3[2]+" "+ arc3[3]+" "+ arc3[4]+" "+ arc3[5]+ " " +arc3[6]+ " "+arc3[7]+ " "+arc3[8]+ " "+arc3[9]+ " "+arc3[10]+ " ");
    }
}
