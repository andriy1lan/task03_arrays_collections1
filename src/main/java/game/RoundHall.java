package game;

import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Set;
import java.util.HashSet;
import java.util.Random;

class RoundHall {
    Player player;
    List<Door> doors;

    Integer [] dnumbers = {1,2,3,4,5,6,7,8,9,10};
    Set<Integer> numbers = new HashSet<Integer>(Arrays.asList(dnumbers));

    public RoundHall (String name) {
        this.player = new Player(name);
        this.doors = new ArrayList();
    }

    public void printDoorsInfo() {
        System.out.println("The name of player is - " + this.player.name);
        System.out.println("Here you can see info about the Doors and targets that are behind them:");
        System.out.println("Door number --- The target behind -- The points:");
        for(Door d:doors) {
            System.out.println("  Door: " + d.number + "------- " + d.findings+" ----------- " + d.dpoints + " ; ");
        }
    }

    public void createTargets() throws InterruptedException {

        int [] bits = new int[10];
        int [] targpoints = new int[10];
        Random r = new Random();
        for(int i = 0; i < 10; i++) {
            bits[i] = (r.nextInt(100))%2;
            Thread.sleep(30);
        }
        for(int i = 0;i < 10; i++) {
            if (bits[i] == Targets.ARTEFACT.ordinal()) targpoints[i] = r.nextInt(70) + 10;
            else if (bits[i] == Targets.MONSTER.ordinal()) targpoints[i] = r.nextInt(95) + 5;
            Thread.sleep(30);
        }
        for(int i = 0;i < 10; i++) {
            Door d = new Door(i+1, Targets.values()[bits[i]], targpoints[i]);
            doors.add(d);
        }
    }
}
