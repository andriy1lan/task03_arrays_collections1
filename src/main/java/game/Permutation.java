package game;

import java.util.List;
import java.util.ArrayList;

class Permutation {

    static List<List<Integer>> list = new ArrayList<List<Integer>>();
    static List<Integer> innerlist;

    public static void createlist (int a[], int n)
    {
        innerlist = new ArrayList<Integer>();
        for (int i = 0; i < n; i++) {
            innerlist.add(a[i]);
            //System.out.print(a[i]+" ");
            // Next - creating the new list of 10 integer to add to outer list
            if (i == (a.length-1)) {  //System.out.println();
                list.add(innerlist);
                innerlist = new ArrayList<Integer>(); //create new empty list of integers to add next set
            }
        }
    }

    public static void swap(int[] a, int i, int j) {
        int temp = a[i];
        a[i] = a[j];
        a[j] = temp;
    }


    public static void permute(int a[], int size, int n)
    {
        // if size becomes 1 then prints the obtained permutation
        if (size == 1)
        {
            createlist(a, n);
            return;
        }
        for (int i=0; i<size; i++)
        {
            permute(a,size-1,n);
            // if size is odd, swap first and last element
            if (size%2==1)
                swap(a,0, size-1);
                // If size is even, swap ith and last element
            else
                swap(a,i, size-1);
        }
    }


    public static void main (String args[]) {
        int [] a = {1,2,3,4,5,6,7,8,9};
        int n = a.length;
        permute(a, n, n);
        System.out.println(list.size() + " ");
        //Runtime r = Runtime.getRuntime();
        //System.out.println(r.totalMemory() + " " + r.freeMemory());
        //System.out.print(list.get(1).get(0) + " " + list.get(1).get(1) + " " + list.get(1).get(2));
    }
}