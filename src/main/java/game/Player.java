package game;

public class Player {
    int points = 25;
    String name;

    public Player(String name) {
        this.name = name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void shoot(Door door) {
        if (door.findings == Targets.ARTEFACT) { this.points += door.dpoints; System.out.println("You get magic points - now you have: " + this.points + " "); }
        else if (door.findings == Targets.MONSTER) {
            System.out.println("You have "+this.points+"points. Monster have "+door.dpoints+" points");
            if (this.points >= door.dpoints) {System.out.println("You won a battle for monster - now you have: "+ this.points + " ");}
            if (this.points < door.dpoints) {System.out.println("You lost a battle for a monster"); this.points = 0;}
        }
    }
}