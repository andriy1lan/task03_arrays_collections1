package game;

import java.util.Scanner;

public class Game {
    String name;
    RoundHall rh;
    public void playGame() {
        System.out.println("Enter you name");
        Scanner sc1 = new Scanner(System.in);
        if (sc1.hasNext()) {
            name = sc1.next();}
        //sc1.close();
        System.out.println("Your name is: "+name);
        RoundHall rh = new RoundHall(name);
        try {
            rh.createTargets();
        }
        catch (InterruptedException ie) {System.out.println(ie.getMessage());}
        rh.printDoorsInfo();

        int num = 0;
        Scanner sc = new Scanner(System.in);
        int k=0;
        try {
            do {
                System.out.println("Please enter the number of door to open");
                while (!sc.hasNextInt()) {
                    System.out.println("That is not a positive number");
                    sc.next(); }
                num = sc.nextInt();
                if (num < 1) {
                    System.out.println("That is correct  number: it needs 1-10"); continue;
                }
                if (num > 10) {
                    System.out.println("That is correct  number: it needs 1-10"); continue;
                }

                if(rh.numbers.isEmpty())  {
                    if(rh.player.points > 0) {
                        System.out.println("You won a game"); }
                }

                if(!rh.numbers.contains(num))  {
                    System.out.println("You have opened this door already"); continue;
                }
                System.out.print("You have opened the door №"+num+" ");
                rh.player.shoot(rh.doors.get(num-1));
                rh.numbers.remove(num);
                k++;
                if (rh.player.points <= 0) {System.out.print("You lost a game"); return;}
                if (k == 10){ if(rh.player.points > 0) {System.out.print("You won a game"); return;}}
            } while (k < 10);
        }
        catch (Exception ex) {
            System.out.print(ex.toString()); }
    }

    public static void main (String args[]) {
        System.out.println("              GAME - ROUND HALL WITH DOORS");
        System.out.print("Do you wanna play the game - Type 'y' or 'Y' \n - otherswise type any other character");
        Game game = new Game();
        Scanner sc = new Scanner(System.in);
        if (sc.hasNext("y") || sc.hasNext("Y")) {
            game.playGame();
        }
        else {System.out.println("You do not want play a game"); return;}
        sc.close();
    }

}
