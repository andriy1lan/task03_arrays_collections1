package droidscollection;

public class Droid {
    String name;

    public Droid(String name)  {
        this.name = name;
    }
    public void setName(String name)  {
        this.name = name;
    }
    public String getName()  {
        return name;
    }
}