package droidscollection;
import java.util.List;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Collections;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class PriorityQueue <Droid> implements Iterable<Droid> {
    Comparator<Droid> droidComparator;
    List<Droid> list;

    public PriorityQueue () {
        list = new ArrayList<Droid>(); //You can use more versalite LinkedList collection to remove elements from the head more easily (swiftly)
        droidComparator = (Comparator<Droid>) new DroidComparator();
    }

    public PriorityQueue (List<Droid> list) {
        this.list = list;
        droidComparator = (Comparator<Droid>) new DroidComparator();
        Collections.sort(this.list,droidComparator);
    }

    public Iterator<Droid> iterator() {
        return list.iterator();
    }

    public void clear()       {
        list = new ArrayList<Droid>();
    }

    public boolean isEmpty()  {
        return list.size() == 0;
    }

    public int size()  {
        return list.size();
    }

    public boolean add(Droid droid) {
        boolean res = list.add(droid);
        Collections.sort(list,droidComparator);
        if (res == true) return true;
        else { throw new IllegalStateException(); }
    }

    public boolean offer(Droid droid) {
        boolean res= list.add(droid);
        Collections.sort(list,droidComparator);
        return res;
    }

    public Droid peek(){
        Collections.sort(list, droidComparator);
        if (list.size() != 0) return list.get(0);
        else return null;
    }

    public Droid poll(){
        Droid result;
        Collections.sort(list, droidComparator);
        if (list.size() != 0) { result = list.get(0); list.remove(result); return result;}
        else return null;
    }

    public boolean remove(){
        Droid result;
        Collections.sort(list, droidComparator);
        if (list.size() != 0) { result = list.get(0); list.remove(result); return true; }
        else throw new NoSuchElementException();
    }

}
