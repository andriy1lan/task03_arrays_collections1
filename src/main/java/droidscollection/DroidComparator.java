package droidscollection;
import java.util.Comparator;

public class DroidComparator implements Comparator<Droid> {

    public int compare(Droid d1, Droid d2) {
        return d1.getName().compareTo(d2.getName());
    }
}