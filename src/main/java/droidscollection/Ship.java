package droidscollection;

import java.util.List;
import java.util.ArrayList;

public class Ship<T> {
    List<T> list;

    public Ship() {
        list = new ArrayList<T>();
    }

    public void putDroid(T droid) {
        list.add(droid);
    }

    public T getDroid(String name) {
        T droid = null;
        for (T d : list) {
            if (((Droid) d).getName().equals(name)) droid = d;
        }
        return droid;
    }

    public List<T> getDroids() {
        List<T> droids = this.list;
        return droids;
    }


    public static void main(String args[]) {
        Ship<Droid> ship = new Ship<Droid>();
        Droid d1 = new Droid("cdroid");
        Droid d2 = new Droid("adroid");
        Droid d3 = new Droid("bdroid");
        try {
            ship.putDroid(d1);
            ship.putDroid(d2);
            ship.putDroid(d3);
            System.out.println("We get out a droid from ship:it is - " + ship.getDroid("bdroid").getName());
        } catch (Exception e) {
        }

        System.out.println();
        PriorityQueue<Droid> pq = new PriorityQueue<Droid>(ship.getDroids());
        pq.offer(new Droid("edroid"));
        pq.add(new Droid("ddroid"));
        System.out.println("Check the workness of iterator of for-cycle in custom PriorityQueue");
        for (Droid d : pq) {
            System.out.println(d.getName());
        }
        System.out.println();
        System.out.println("We check the priorities of elements inserted randomly in custom PriorityQueue");
        System.out.println(pq.poll().getName());
        System.out.println(pq.poll().getName());
        System.out.println(pq.poll().getName());
        System.out.println(pq.poll().getName());
        System.out.println(pq.poll().getName());
        //for(Droid d:ship.getDroids()) {System.out.println(d.getName());}

        //The next attempt
        List<Integer> intList = new ArrayList<Integer>();
        intList.add(2);
        //try {intList.add("String_one"); --- Compiler error when adding string to List<Integer>
        //}
        //catch(Exception e) {System.out.println(e.getCause().getMessage());};
        //

    }
}